
var keyCard;

function init() {
    initFirebase();
    keyCard = new URLSearchParams(window.location.search).get('keyCard');
    if (keyCard != "" && keyCard != undefined && keyCard != null) {
        loadInformationsCard(keyCard);
    }

}

//Carga toda la informacion de la tarjeta
function loadInformationsCard(keyCard) {
    database.ref("tarjetas/" + keyCard).once("value", function (card) {
        //Imagen Principal
        document.getElementById("imgMain").src = card.child("imagenes").val();

        //Autor
        setInfoField(card, "autor", "author", card.val().autor);

        //Nombre
        setInfoField(card, "nombre", "name", card.val().nombre);

        //Apellido
        setInfoField(card, "apellido", "surname", card.val().apellido);

        //Apodo
        setInfoField(card, "apodo", "nickname", card.val().apodo);

        //Edad
        setInfoField(card, "edad", "age", card.val().edad);

        //Sexo
        setInfoField(card, "sexo", "gender", card.val().sexo);

        //Raza
        setInfoField(card, "raza", "race", card.val().raza);

        //Nivel 
        setInfoField(card, "nivel", "level", card.val().nivel);

        //Fabula
        setInfoField(card, "fabula", "fable", card.val().fabula);

        //Especie
        setInfoField(card, "especie", "specimen", card.val().especie);

        //Nutricion
        setInfoField(card, "nutricion", "eat", card.val().nutricion);

        //Tamaño
        setInfoField(card, "tamaño", "size", card.val().tamaño);

        //Habitat
        setInfoField(card, "habitat", "habitat", card.val().habitat);

        //Rareza
        setInfoField(card, "rareza", "rare", card.val().rareza);

        //Estado
        setInfoField(card, "estado", "state", card.val().estado);

        //Mundo
        setInfoField(card, "mundo", "world", card.val().mundo);

        //Ocupacion
        setInfoField(card, "ocupacion", "job", card.val().ocupacion);

        //Pertenencias
        setInfoField(card, "pertenencias", "items", card.val().pertenencias);

        //Descripcion
        setInfoField(card, "descripcion", "description", card.val().descripcion);

        //Descripcion
        setInfoField(card, "habilidadesespeciales", "SpecialHability", card.val().habilidadesespeciales);

        //Modus Operandi
        setInfoField(card, "modusoperandi", "moperandi", card.val().modusoperandi);

        //Historia
        setInfoField(card, "historia", "history", card.val().historia);

        //Etiquetas
        setInfoField(card, "etiqueta", "tags", card.val().etiqueta);

        //Usuario
        setUserField(card);

    });
}

//escribe el valor'Value' con nombre'nameValue' de la rama'card' en sus campos'keyField'
//y hace invisibles el campos sin no existe dicha informacion
function setInfoField(card, nameValue, keyField, value) {
    //<a - ===> <a href='URL
    //URL=
    //file:///D:/Mis Cosas/Proyectos Software/Blog_0asis/blogRol/cardPage.html?keyCard=-
    //https://blogrol-0asis.firebaseapp.com/cardPage.html?keyCard=-

    if (card.hasChild(nameValue)) {
        while (value.includes("<a -")) {
            value = value.replace("<a -", "<a href='https://blogrol-0asis.firebaseapp.com/cardPage.html?keyCard=-");
        }
        document.getElementById(keyField + "Field").innerHTML += value + ".";

    } else {
        document.getElementById(keyField + "Div").hidden = true;
    }
}

//escribe el usuario como si fuera una etiqueta
function setUserField(card) {
    if (card.hasChild("usuario")) {
        document.getElementById("tagsDiv").hidden = false;
        database.ref("usuarios/" + card.val().usuario).once("value", function (user) {
            document.getElementById("tagsField").innerHTML = document.getElementById("tagsField").innerHTML.slice(0, -1);
            document.getElementById("tagsField").innerHTML += user.val().apodo + ".";
        });
    }
}
