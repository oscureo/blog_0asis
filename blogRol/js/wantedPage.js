
function init() {
    initFirebase();
    loadWanteds();
}

//Carga los wanteds de la base de datos
function loadWanteds() {
    database.ref("Wanted").once("value", function (data) {
        var activeWanted = true;
        var cont = 0;
        while (cont < 2) {
            var refwanted;
            var list;
            if (activeWanted) {
                refwanted = data.child("Activo");
                list = document.getElementById("ActiveWantedList");
            } else {
                refwanted = data.child("Inactivo");
                list = document.getElementById("InactiveWantedList");
            }
            refwanted.forEach(function (wanted) {
                list.innerHTML += '<div>\
                <img src="'+ wanted.val().imagen + '" width="100%" />\
                <p>'+ wanted.val().texto + '</p>\
                </div><br/><br/>';
            });
            activeWanted = false;
            cont++;
        }
    });
}