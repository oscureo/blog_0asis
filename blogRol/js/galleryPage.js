

function init() {
    drag = false;
    document.addEventListener("webkitfullscreenchange", function (event) {
        changeFullScreen();
    });

    document.addEventListener("mozfullscreenchange", function (event) {
        changeFullScreen();
    });

    document.getElementById("divbody2").height = "200px";

}

//guarda la imagen recien escogida 'input' en 'output'
function saveImg(input, output) {
    //imgForm = input.files[0];

    var reader = new FileReader();
    reader.onload = function (e) {
        var img = document.getElementById(output);
        img.src = this.result;
        var differenceWidth = img.parentNode.width - img.width;
        var differenceHeight = img.parentNode.height - img.height;
        if (differenceWidth > differenceHeight) {
            img.style.height = "100%";
        } else {
            img.style.width = "100%";
        }
    };
    reader.readAsDataURL(input.files[0]);

}

// Lanza en pantalla completa en navegadores que lo soporten
function launchFullScreen(element) {

    if (element.requestFullScreen) {
        element.requestFullScreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
    }
}

// Modifica la ventana para el formato fullScreen o Screen
function changeFullScreen() {
    if (document.getElementById("headboard").hidden) {
        //Modo Screen
        document.getElementById("headboard").hidden = false;
        document.getElementById("paddingDiv").hidden = false;
        document.getElementById("btnFullScreen").hidden = false;
    } else {
        //Modo FullScreen
        document.getElementById("headboard").hidden = true;
        document.getElementById("paddingDiv").hidden = true;
        document.getElementById("btnFullScreen").hidden = true;
    }
}