
var database;

//Inicia la conexion con firebase
function initFirebase() {
    const firebaseConfig = {
        apiKey: "AIzaSyBQNPee7k58S3gEt8xkqMHxC559CZ2d3ks",
        authDomain: "blogrol-0asis.firebaseapp.com",
        databaseURL: "https://blogrol-0asis.firebaseio.com",
        projectId: "blogrol-0asis",
        storageBucket: "blogrol-0asis.appspot.com",
        messagingSenderId: "573588456554",
        appId: "1:573588456554:web:806fd1a97ef42871"
    };
    firebase.initializeApp(firebaseConfig);
    database=firebase.database();
}

//Muestra el menu lateral
function showLeftMenu() {
    document.getElementById("leftMenu").hidden = false;
}

//Oculta el menu lateral
function hideLeftMenu() {
    document.getElementById("leftMenu").hidden = true;
}

//Direge hacia la pagina de listas de tarjetas
function gotoMainPage(filtro) {
    if (filtro == undefined || filtro == null) {
        filtro = "";
    }
    window.open("index.html?filtro=" + filtro, "_self");
}

//Dirige hacia la pagina de mapa
function gotoMapPage() {
    window.open("mapPage.html", "_self");
}

//Direge hacia la pagina de se buscas
function gotoWantedPage() {
    window.open("wantedPage.html", "_self");
}

//Direge hacia la pagina de  galeria
function gotoGalleryPage() {
    window.open("GalleryPage.html", "_self");
}

//Devuelve el titulo de la tarjeta segun su contenido
function getTitleCard(card) {
    if (card.hasChild("especie")) {
        return card.val().especie;
    } else if (card.hasChild("apodo")) {
        return card.val().apodo;
    } else if (card.hasChild("apellido")) {
        return card.val().nombre + " " + card.val().apellido;
    } else {
        return card.val().nombre;
    }
}

//Devuelve una clave de fecha invertida
function getKeyBD() {
    var key = "-";
    var d = new Date();
    key += (4000 - d.getFullYear()) + "-";

    var month = 13 - d.getMonth();
    if (month < 10) month = "0" + month;
    key += month + "-";

    var day = 40 - d.getDate();
    if (day < 10) day = "0" + day;
    key += day + "-";

    var hour = 60 - d.getHours();
    if (hour < 10) hour = "0" + hour;
    key += hour + ":";

    var minute = 60 - d.getMinutes();
    if (minute < 10) minute = "0" + minute;
    key += minute + ":";

    var second = 60 - d.getSeconds();
    if (second < 10) second = "0" + second;
    key += second;
    return key;
}