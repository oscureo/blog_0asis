
var imgForm;//Guarda la ultima foto subida desde el formulario de tarjetas
var keyCardEdit;
var oldTags;
var oldTypeWanted;
var oldUser;

function init() {
    var rightValue = false;
    var cont = 0;
    while (!rightValue && cont < 5) {
        var pin = prompt("Por favor, introduzca el PIN", "");
        if ("101112" === pin) {
            rightValue = true;
            initFirebase();
            keyCardEdit = "";
            oldTags = "";
            oldUser = "";
            loadCardTags();
            var btns = document.getElementById("btnFormsList").getElementsByTagName("input");
            for (var i = 0; i < btns.length; i++) {
                btns[i].disabled = false;
            }
        }
        cont++;
    }
}

//muestra el formulario de tarjetas
function showEmptyForm() {
    if (document.getElementById("formCard").hidden) {
        document.getElementById("formCard").hidden = false;
        document.getElementById("cardsListDiv").hidden = true;
        document.getElementById("formWanted").hidden = true;
    }
    document.getElementById("btnsave").disabled = false;
    document.getElementById("btnPublishForm").disabled = false;
    document.getElementById("btnsave").hidden = false;
    document.getElementById("btnedit").hidden = true;
    document.getElementById("divKey").hidden = true;
    keyCardEdit = "";
    oldTags = "";
    oldUser = "";
    //limpiar formulario
    document.getElementById("imgForm").src = "";
    imgForm = undefined;
    document.getElementById("inputKey").value = "";
    document.getElementById("inputName").value = "";
    document.getElementById("inputSurname").value = "";
    document.getElementById("inputNickname").value = "";
    document.getElementById("inputAge").value = "";
    document.getElementById("inputGender").selectedIndex = 0;
    document.getElementById("inputRace").value = "";
    document.getElementById("inputLevel").selectedIndex = 0;
    document.getElementById("inputFable").value = "";
    document.getElementById("inputSpecimen").value = "";
    document.getElementById("inputEat").value = "";
    document.getElementById("inputSize").value = "";
    document.getElementById("inputHabitat").value = "";
    document.getElementById("inputRare").value = "";
    document.getElementById("inputState").value = "";
    document.getElementById("inputWorld").value = "";
    document.getElementById("inputJob").value = "";
    document.getElementById("inputItems").value = "";
    document.getElementById("inputDescription").value = "";
    document.getElementById("inputSpecialHability").value = "";
    document.getElementById("inputMOperandi").value = "";
    document.getElementById("inputHistory").value = "";
    document.getElementById("inputType").selectedIndex = 0;
    document.getElementById("inputAuthor").value = "";

    var checkeds = document.getElementById("inputTags").getElementsByTagName("label");
    var i = 0;
    while (i < checkeds.length) {
        checkeds[i].childNodes[0].checked = false;
        i++;
    }

    var listUser = document.getElementById("inputUsers");
    if (listUser.options.length == 1) {
        database.ref("usuarios").once("value", function (data) {
            data.forEach(function (user) {
                if (user.val.apodo != "") {
                    var option = document.createElement("option");
                    option.value = user.key;
                    option.textContent = user.val().apodo;
                    listUser.add(option);
                }
            });
        });
    }
}

//Muestra la tabla de tarjetas
function showListCards() {
    if (document.getElementById("cardsListDiv").hidden) {
        document.getElementById("cardsListDiv").hidden = false;
        document.getElementById("formCard").hidden = true;
        document.getElementById("formWanted").hidden = true;
        loadtableCards();
    }
}

//Escribe Nada importante en el campo de pertenencias
function writeEmptyItemsWord() {
    document.getElementById("inputItems").value = "Nada importante";
}

//guarda la imagen recien escogida
function saveImg(input, output) {
    imgForm = input.files[0];

    var reader = new FileReader();
    reader.onload = function (e) {
        document.getElementById(output).src = this.result;
    };
    reader.readAsDataURL(imgForm);

}

//Crea una tarjeta en la BD
function saveCard() {

    if (imgForm != undefined && imgForm.toString().trim() != "") {
        var name = document.getElementById("inputName").value;
        var especie = document.getElementById("inputSpecimen").value;
        if ((name != undefined && name.trim() != "") || (especie != undefined && especie.trim() != "")) {
            document.getElementById("btnsave").disabled = true;
            document.getElementById("btnPublishForm").disabled = true;
            //Guardamos Tarjeta
            var type = document.getElementById("inputType").options[document.getElementById("inputType").selectedIndex].value;
            var key = getKeyBD();

            firebase.storage().ref(type + "/" + imgForm.name).put(imgForm).then(function (dataimg) {

                dataimg.ref.getDownloadURL().then(function (url) {
                    var urlImg = url.toString();
                    var tags = "-" + document.getElementById("inputType").value + "-";
                    var checkeds = document.getElementById("inputTags").getElementsByTagName("label");
                    var i = 0;
                    while (i < checkeds.length) {
                        if (checkeds[i].childNodes[0].checked) {
                            tags += checkeds[i].childNodes[0].value + "-";
                        }
                        i++;
                    }
                    var cardRef = "tarjetas/";
                    if (document.getElementById("btnPublishForm").value == "No") {
                        cardRef = "tarjetasNoPublicadas/";
                    }

                    database.ref(cardRef + key).set({
                        nombre: getInputForm(name),
                        apellido: getInputForm(document.getElementById("inputSurname").value),
                        apodo: getInputForm(document.getElementById("inputNickname").value),
                        edad: getInputForm(document.getElementById("inputAge").value),
                        sexo: getSelectForm("inputGender"),
                        raza: getInputForm(document.getElementById("inputRace").value),
                        nivel: getSelectForm("inputLevel"),
                        fabula: getInputForm(document.getElementById("inputFable").value),
                        especie: getInputForm(especie),
                        nutricion: getInputForm(document.getElementById("inputEat").value),
                        tamaño: getInputForm(document.getElementById("inputSize").value),
                        habitat: getInputForm(document.getElementById("inputHabitat").value),
                        rareza: getInputForm(document.getElementById("inputRare").value),
                        estado: getInputForm(document.getElementById("inputState").value),
                        mundo: getInputForm(document.getElementById("inputWorld").value),
                        ocupacion: getInputForm(document.getElementById("inputJob").value),
                        pertenencias: getInputForm(document.getElementById("inputItems").value),
                        descripcion: getInputForm(document.getElementById("inputDescription").value),
                        habilidadesespeciales: getInputForm(document.getElementById("inputSpecialHability").value),
                        modusoperandi: getInputForm(document.getElementById("inputMOperandi").value),
                        historia: getInputForm(document.getElementById("inputHistory").value),
                        usuario: getSelectForm("inputUsers"),
                        etiqueta: tags,
                        imagenes: {
                            "0": urlImg
                        },
                        autor: getInputForm(document.getElementById("inputAuthor").value),
                    }).then(function (v) {

                        var arrayPromise = new Array();

                        var newUser = getSelectForm("inputUsers");
                        arrayPromise.push(new Promise(function (resolve) {
                            if (newUser != null) {
                                database.ref("usuarios/" + newUser + "/tarjetas/" + key).set(" ").then(function (v) {
                                resolve();
                            });
                            }else{
                                resolve();
                            }
                        }));

                        if (document.getElementById("btnPublishForm").value == "Si") {
                            var arrayTags = tags.split("-");
                            var i = 1;
                            
                            while (i < (arrayTags.length - 1)) {
                                arrayPromise.push(new Promise(function (resolve) {
                                    database.ref("FiltrosPorTipo/" + arrayTags[i] + "/" + key).set(" ").then(function (v) {
                                        resolve();
                                    });
                                }));
                                i++;
                            }
                        } 
                        Promise.all(arrayPromise).then(function (value) {
                            alert("Tarjeta Creada con Exito");
                        });
                        
                    });
                });
            });
        } else {
            alert("El nombre de la tarjeta es obligatorio");
        }
    } else {
        alert("La imagen de la tarjeta es obligatoria");
    }
}


//devuelve null si en un texto no valido
function getInputForm(text) {
    if (text != undefined && text.trim() != "") {
        return text;
    } else {
        return null;
    }
}

//devuelve null si en un select no valido
function getSelectForm(id) {
    var text = document.getElementById(id).options[document.getElementById(id).selectedIndex].value;
    if (text != undefined && text.trim() != "" && text != "Nada") {
        return text;
    } else {
        return null;
    }
}

//Carga las tarjetas en la tabla
function loadtableCards() {
    document.getElementById("loadImg").hidden = false;
    var cont = 0;
    var tableCards = document.getElementById("tableCardsBody");
    var cardRef = database.ref("tarjetas");
    var promiseCard = new Promise(function (resolve) {

        cardRef.off();
        cardRef.once("value", function (data) {

            //Rellenar tabla tarjetas
            tableCards.innerHTML = "";
            data.forEach(function (card) {
                var nextCelda = tableCards.insertRow(-1).insertCell(0);
                nextCelda.innerHTML = '<div>\
                <div style="float:left;">'+ getTitleCard(card) + '</div>\
                <div style="float:right;">\
                <input type="button" value="Editar" class="btn btn-success" onclick="loadEditCard(\'' + card.key + '\')" />\
                <input type="button" value="Ocultar" class="btn btn-secondary" onclick="HidePublishCard(\'' + card.key + '\')" />\
                <input type="button" value="Borrar" class="btn btn-danger" onclick="btnRemoveCard(\'' + card.key + '\')" />\
                </div>\
                </div>';
                if (cont % 2 == 0) {
                    //Poner el fondo en azul
                    nextCelda.style.backgroundColor = "cornflowerblue";
                }
                cont++;
            });
            resolve();
        });
    }).then(function (v) {
        //Tarjetas No publicadas
        var cardNotPublishRef = database.ref("tarjetasNoPublicadas");
        cardNotPublishRef.off();
        cardNotPublishRef.once("value", function (data) {

            //Rellenar tabla tarjetas no publicadas
            data.forEach(function (card) {
                var nextCelda = tableCards.insertRow(-1).insertCell(0);
                nextCelda.innerHTML = '<div>\
                <div style="float:left;">\
                <div>' + getTitleCard(card) + '</div>\
                </div>\
                <div style="float:right;">\
                <input type="button" value="Publicar" class="btn btn-warning" onclick="publishCard(\'' + card.key + '\')" />\
                </div>\
                </div>';
                if (cont % 2 == 0) {
                    //Poner el fondo en azul
                    nextCelda.style.backgroundColor = "cornflowerblue";
                }
                cont++;
            });
            document.getElementById("loadImg").hidden = true;
        });
    });
}

//carga el formulario con la tarjeta seleccionada
function loadEditCard(idCard) {

    showEmptyForm();
    database.ref("tarjetas/" + idCard).once("value", function (card) {

        if (card.hasChild("imagenes/0")) document.getElementById("imgForm").src = card.child("imagenes/0").val();

        document.getElementById("inputKey").value = card.key;

        if (card.hasChild("nombre")) document.getElementById("inputName").value = card.val().nombre;

        if (card.hasChild("apellido")) document.getElementById("inputSurname").value = card.val().apellido;

        if (card.hasChild("apodo")) document.getElementById("inputNickname").value = card.val().apodo;

        if (card.hasChild("edad")) document.getElementById("inputAge").value = card.val().edad;

        if (card.hasChild("sexo")) {
            switch (card.val().sexo) {
                case ("Hombre"): document.getElementById("inputGender").selectedIndex = 1; break;
                case ("Mujer"): document.getElementById("inputGender").selectedIndex = 2; break;
                case ("Hermafrodita"): document.getElementById("inputGender").selectedIndex = 3; break;
                case ("Desconocido"): document.getElementById("inputGender").selectedIndex = 4; break;
                default: document.getElementById("inputGender").selectedIndex = 0; break;
            }
        }

        if (card.hasChild("raza")) document.getElementById("inputRace").value = card.val().raza;

        if (card.hasChild("nivel")) {
            switch (card.val().nivel) {
                case ("1"): document.getElementById("inputLevel").selectedIndex = 1; break;
                case ("1+"): document.getElementById("inputLevel").selectedIndex = 2; break;
                case ("2"): document.getElementById("inputLevel").selectedIndex = 3; break;
                case ("2+"): document.getElementById("inputLevel").selectedIndex = 4; break;
                case ("3"): document.getElementById("inputLevel").selectedIndex = 5; break;
                case ("3+"): document.getElementById("inputLevel").selectedIndex = 6; break;
                case ("4"): document.getElementById("inputLevel").selectedIndex = 7; break;
                case ("4+"): document.getElementById("inputLevel").selectedIndex = 8; break;
                case ("5"): document.getElementById("inputLevel").selectedIndex = 9; break;
                default: document.getElementById("inputLevel").selectedIndex = 0; break;
            }
        }

        if (card.hasChild("fabula")) document.getElementById("inputFable").value = card.val().fabula;

        if (card.hasChild("especie")) document.getElementById("inputSpecimen").value = card.val().especie;

        if (card.hasChild("nutricion")) document.getElementById("inputEat").value = card.val().nutricion;

        if (card.hasChild("tamaño")) document.getElementById("inputSize").value = card.val().tamaño;

        if (card.hasChild("habitat")) document.getElementById("inputHabitat").value = card.val().habitat;

        if (card.hasChild("rareza")) document.getElementById("inputRare").value = card.val().rareza;

        if (card.hasChild("estado")) document.getElementById("inputState").value = card.val().estado;

        if (card.hasChild("mundo")) document.getElementById("inputWorld").value = card.val().mundo;

        if (card.hasChild("ocupacion")) document.getElementById("inputJob").value = card.val().ocupacion;

        if (card.hasChild("pertenencias")) document.getElementById("inputItems").value = card.val().pertenencias;

        if (card.hasChild("descripcion")) document.getElementById("inputDescription").value = card.val().descripcion;

        if (card.hasChild("habilidadesespeciales")) document.getElementById("inputSpecialHability").value = card.val().habilidadesespeciales;

        if (card.hasChild("modusoperandi")) document.getElementById("inputMOperandi").value = card.val().modusoperandi;

        if (card.hasChild("historia")) document.getElementById("inputHistory").value = card.val().historia;

        if (card.hasChild("usuario")) {
            //Seleccionar el usuario
            database.ref("usuarios/" + card.val().usuario).once("value", function (data) {
                var options = document.getElementById("inputUsers").options;
                var cont = 0;
                var found = false;
                while (!found && cont < options.length) {
                    if (data.val().apodo != "" && data.val().apodo == options[cont].textContent) {
                        document.getElementById("inputUsers").options[cont].selected = "selected";
                        found = true;
                    }
                    cont++

                }
                if (!found) {
                    document.getElementById("inputUsers").options[0].selected = "selected";
                }
            });

        } else {
            document.getElementById("inputUsers").options[0].selected = "selected";
        }

        if (card.hasChild("etiqueta")) {
            if (card.val().etiqueta.includes("-Personaje-")) {
                document.getElementById("inputType").selectedIndex = 0;
            } else if (card.val().etiqueta.includes("-Artefacto-")) {
                document.getElementById("inputType").selectedIndex = 1;
            } else if (card.val().etiqueta.includes("-Criatura-")) {
                document.getElementById("inputType").selectedIndex = 2;
            }
        }

        if (card.hasChild("autor")) document.getElementById("inputAuthor").value = card.val().autor;

        var checkeds = document.getElementById("inputTags").getElementsByTagName("label");
        var i = 0;
        while (i < checkeds.length) {

            checkeds[i].childNodes[0].checked = card.val().etiqueta.includes("-" + checkeds[i].childNodes[0].value + "-");
            i++;
        }

        document.getElementById("btnsave").hidden = true;
        document.getElementById("btnedit").hidden = false;
        document.getElementById("divKey").hidden = false;
        keyCardEdit = idCard;
        oldTags = card.val().etiqueta;
        if (card.hasChild("usuario")) {
            oldUser = card.val().usuario;
        } else {
            oldUser = "";
        }

    });
}

//Edita la tarjeta seleccionada con los datos del formulario
function editCard() {
    var name = document.getElementById("inputName").value;
    var especie = document.getElementById("inputSpecimen").value;
    if ((name != undefined && name.trim() != "") || (especie != undefined && especie.trim() != "")) {
        //Guardamos Tarjeta
        //No se guarda ni la imagenes ni la etiqueta
        var tags = "-" + document.getElementById("inputType").value + "-";
        var checkeds = document.getElementById("inputTags").getElementsByTagName("label");
        var i = 0;
        while (i < checkeds.length) {
            if (checkeds[i].childNodes[0].checked) {
                tags += checkeds[i].childNodes[0].value + "-";
            }
            i++;
        }
        database.ref("tarjetas/" + keyCardEdit).update({
            nombre: getInputForm(name),
            apellido: getInputForm(document.getElementById("inputSurname").value),
            apodo: getInputForm(document.getElementById("inputNickname").value),
            edad: getInputForm(document.getElementById("inputAge").value),
            sexo: getSelectForm("inputGender"),
            raza: getInputForm(document.getElementById("inputRace").value),
            nivel: getSelectForm("inputLevel"),
            fabula: getInputForm(document.getElementById("inputFable").value),
            especie: getInputForm(especie),
            nutricion: getInputForm(document.getElementById("inputEat").value),
            tamaño: getInputForm(document.getElementById("inputSize").value),
            habitat: getInputForm(document.getElementById("inputHabitat").value),
            rareza: getInputForm(document.getElementById("inputRare").value),
            estado: getInputForm(document.getElementById("inputState").value),
            mundo: getInputForm(document.getElementById("inputWorld").value),
            ocupacion: getInputForm(document.getElementById("inputJob").value),
            pertenencias: getInputForm(document.getElementById("inputItems").value),
            descripcion: getInputForm(document.getElementById("inputDescription").value),
            habilidadesespeciales: getInputForm(document.getElementById("inputSpecialHability").value),
            modusoperandi: getInputForm(document.getElementById("inputMOperandi").value),
            historia: getInputForm(document.getElementById("inputHistory").value),
            autor: getInputForm(document.getElementById("inputAuthor").value),
            usuario: getSelectForm("inputUsers"),
            etiqueta: tags
        }).then(function (v) {
            var arrayTags = tags.split("-");
            var i = 1;
            var arrayPromise = new Array();
            var filterRef = database.ref("FiltrosPorTipo");
            while (i < (arrayTags.length - 1)) {
                arrayPromise.push(new Promise(function (resolve) {
                    if (!oldTags.includes(arrayTags[i])) {
                        filterRef.child(arrayTags[i] + "/" + keyCardEdit).set(" ").then(function (v) {
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                }));
                i++;
            }
            var arrayOldTags = oldTags.split("-");
            var j = 1;
            while (j < (arrayOldTags.length - 1)) {
                arrayPromise.push(new Promise(function (resolve) {
                    if (!arrayTags.includes(arrayOldTags[j])) {

                        filterRef.child(arrayOldTags[j] + "/" + keyCardEdit).remove().then(function (v) {
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                }));
                j++;
            }

            var newUser = getSelectForm("inputUsers");
            if (oldUser != newUser) {
                arrayPromise.push(new Promise(function (resolve) {
                    if (newUser != null) {
                        database.ref("usuarios/" + newUser + "/tarjetas/" + keyCardEdit).set(" ").then(function (v) {
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                }));
                arrayPromise.push(new Promise(function (resolve) {
                    if (oldUser != "") {
                        database.ref("usuarios/" + oldUser + "/tarjetas/" + keyCardEdit).remove().then(function (v) {
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                }));
            }

            Promise.all(arrayPromise).then(function (value) {
                oldTags = tags;
                oldUser = newUser;
                alert("Tarjeta Actualizada con Exito");
            });
        });

    } else {
        alert("El nombre de la tarjeta es obligatorio");
    }
}

//Carga las etiquetas seleccionables en el formulario de tarjeta
function loadCardTags() {
    var tagsList = document.getElementById("inputTags");
    if (tagsList.childNodes.length < 2) {
        database.ref("FiltrosPorTipo").once("value", function (data) {
            data.forEach(function (tag) {
                if (tag.key != "Personaje" && tag.key != "Artefacto" && tag.key != "Criatura") {
                    tagsList.innerHTML += '<label class="checkbox-inline"><input type="checkbox" value="' + tag.key + '">' + tag.key + '</label>';
                }
            });
        });
    }
}

//Crea una nueva opcion de etiqueta en el formulario
function addCardTag() {
    var tagsList = document.getElementById("inputTags");
    var word = document.getElementById("inputNewTag").value;
    tagsList.innerHTML += '<label class="checkbox-inline"><input type="checkbox" value="' + word + '">' + word + '</label>';
}

//Funcion que pregunta antes de borrar una tarjeta de la BD
async function btnRemoveCard(idCard) {
    if (confirm("Estas apunto de borrar la tarjeta " + idCard)) {
        removeCard(idCard);
    }
}

//borra la tarjeta de la BD
function removeCard(idCard) {
    database.ref("tarjetas/" + idCard + "/etiqueta").once("value", function (data) {
        var arrayPromise = new Array();
        var tags = data.val().split("-");
        for (var cont = 1; cont < tags.length - 1; cont++) {
            arrayPromise.push(new Promise(function (resolve) {
                database.ref("FiltrosPorTipo/" + tags[cont] + "/" + idCard).remove()
                    .then(function (v) { resolve(); });
            }));
        }
        arrayPromise.push(new Promise(function (resolve) {
            if (data.hasChild("usuario")) {
                database.ref("usuarios/" + data.val().usuario + "/tarjetas/" + idCard).remove().then(function (v) {
                    resolve();
                });
            }
        }));
        Promise.all(arrayPromise).then(function (v) {
            database.ref("tarjetas/" + idCard).remove().then(function (v) {
                loadtableCards();
                console.log("Accion finalizada con exito");
            });
        });
    });
}

//Cambia el boton de publicar a no publicar en el formulario tarjeta
function turnBtnPublish() {
    var btn = document.getElementById("btnPublishForm");
    if (btn.value == "Si") {
        btn.value = "No";
        btn.classList.remove("btn-success");
        btn.classList.add("btn-danger");
    } else {
        btn.value = "Si";
        btn.classList.remove("btn-danger");
        btn.classList.add("btn-success");
    }
}

//Publica la tarjeta en el blog para que sea visible por usuarios
function publishCard(key) {
    var tags;
    database.ref("tarjetasNoPublicadas/" + key).once("value", function (data) {
        tags = data.val().etiqueta;
        database.ref("tarjetas/" + key).set(
            data.val()
        ).then(function (v) {
            var arrayPromise = new Array();
            var arrayTags = tags.split("-");
            var i = 1;
            while (i < (arrayTags.length - 1)) {
                arrayPromise.push(new Promise(function (resolve) {
                    database.ref("FiltrosPorTipo/" + arrayTags[i] + "/" + key).set(" ").then(function (v) {
                        resolve();
                    });
                }));
                i++;
            }
            arrayPromise.push(new Promise(function (resolve) {
                if (data.hasChild("usuario")) {
                    database.ref("usuarios/" + data.val().usuario + "/tarjetas/" + key).set(" ").then(function (v) {
                        resolve();
                    });
                }
            }));
            Promise.all(arrayPromise).then(function (value) {
                database.ref("tarjetasNoPublicadas/" + key).remove().then(function (v) {
                    loadtableCards();
                    alert("tarjeta publicada con exito");
                });
            });
        });
    });

}


//Oculta la tarjeta para que No sea visible por usuarios
function HidePublishCard(key) {
    if (confirm("¿Ocultar tarjeta " + key + " ?")) {
        database.ref("tarjetas/" + key).once("value", function (data) {
            database.ref("tarjetasNoPublicadas/" + key).set(
                data.val()
            ).then(function (v) {
                removeCard(key);
            });
        });
    }
}

//Filtra la lista de tarjetas por palabra clave
function filterTableCard() {
    var textfilter = document.getElementById("serachTableCard").value;
    var rows = document.getElementById("tableCardsBody").getElementsByTagName("tr");
    var colorCont = 0;
    for (i = 0; i < rows.length; i++) {
        var td = rows[i].getElementsByTagName("td")[0];
        if (td) {
            if (textfilter.trim() != "") {
                var tdText = td.getElementsByTagName("div")[0].getElementsByTagName("div")[0].innerHTML;
                tdText = tdText.toLowerCase();
                if (tdText.includes(textfilter.toLowerCase())) {
                    //Mostrar
                    rows[i].style.display = "";
                    if (colorCont % 2 == 0) {
                        td.style.backgroundColor = "cornflowerblue";
                    } else {
                        td.style.backgroundColor = "#17212b";
                    }
                    colorCont++;
                } else {
                    //Ocultar
                    rows[i].style.display = "none";
                }
            } else {
                //Mostrar
                rows[i].style.display = "";
                if (colorCont % 2 == 0) {
                    td.style.backgroundColor = "cornflowerblue";
                } else {
                    td.style.backgroundColor = "#17212b";
                }
                colorCont++;
            }
        }
    }
}

//------------WANTED----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

//limpiar el formulario de wanteds y lo muestra
function showEmptyWantedForm() {
    if (document.getElementById("formWanted").hidden) {
        document.getElementById("formWanted").hidden = false;
        document.getElementById("cardsListDiv").hidden = true;
        document.getElementById("formCard").hidden = true;
        if (document.getElementById("tableWantedsBody").childNodes.length < 1) {
            loadtableWanteds();
        }
    }
    //limpiar formulario
    document.getElementById("imgFormWanted").src = "";
    imgForm = undefined;
    oldTypeWanted = "";
    document.getElementById("inputWatedKey").value = "";
    document.getElementById("inputWantedText").value = "";
}

//Guarda el wanted recogiendo la informacion del formulario
function saveWanted() {
    if (imgForm != undefined || document.getElementById("imgFormWanted").src != "") {
        var text = document.getElementById("inputWantedText").value;
        if (text != undefined && text.trim() != "") {
            //Guardamos el Wanted
            var key;
            if (document.getElementById("inputWatedKey").value == "") {
                key = getKeyBD();
                firebase.storage().ref("Wanted/" + imgForm.name).put(imgForm).then(function (dataimg) {

                    dataimg.ref.getDownloadURL().then(function (url) {
                        var urlImg = url.toString();

                        database.ref("Wanted/" + document.getElementById("inputWantedOn").value + "/" + key).set({
                            texto: text,
                            imagen: urlImg
                        }).then(function (v) {
                            alert("Wanted Creado con Exito");
                        });
                    });
                });
            } else {
                key = document.getElementById("inputWatedKey").value;
                if (oldTypeWanted != document.getElementById("inputWantedOn").value) {
                    //Hay que borrar la rama anterior y crear la nueva
                    database.ref("Wanted/" + oldTypeWanted + "/" + key).remove().then(function (v) {

                        database.ref("Wanted/" + document.getElementById("inputWantedOn").value + "/" + key).update({
                            texto: text,
                            imagen: document.getElementById("imgFormWanted").src
                        }).then(function (v) {
                            alert("Wanted Creado con Exito");
                        });
                    });
                } else {
                    database.ref("Wanted/" + document.getElementById("inputWantedOn").value + "/" + key).update({
                        texto: text
                    }).then(function (v) {
                        alert("Wanted Creado con Exito");
                    });
                }
            }

        } else {
            alert("El texto es obligatorio");
        }
    } else {
        alert("La imagen es obligatoria");
    }
}

//Carga las tarjetas en la tabla
function loadtableWanteds() {
    var cardRef = database.ref("Wanted");
    cardRef.off();
    cardRef.on("value", function (data) {
        var tableWanteds = document.getElementById("tableWantedsBody");
        tableWanteds.innerHTML = "";
        var cont = 0;
        var firts = true;
        var types = "Activo";
        var cycle = 0;
        while (cycle < 2) {

            if (!firts) types = "Inactivo";
            //Rellenar tabla wanted
            data.child(types).forEach(function (wanted) {

                var nextCelda = tableWanteds.insertRow(-1).insertCell(0);
                nextCelda.innerHTML = '<div>\
                <div style="float:left;">\
                <div>' + wanted.key + '</div>\
                </div>\
                <div style="float:right;">\
                <input type="button" value="Editar" class="btn btn-success" onclick="loadEditWanted(\'' + types + '\',\'' + wanted.key + '\')" />\
                <input type="button" value="Borrar" class="btn btn-danger" onclick="removeWanted(\'' + types + '\',\'' + wanted.key + '\')" />\
                </div>\
                </div>';
                if (cont % 2 == 0) {
                    //Poner el fondo en azul
                    nextCelda.style.backgroundColor = "cornflowerblue";
                }
                cont++;
            });
            cycle++;
            firts = false;
        }
    });
}

//carga en el formulario los datos del wanted
function loadEditWanted(type, key) {

    document.getElementById("inputWatedKey").value = key;
    oldTypeWanted = type;

    database.ref("Wanted/" + type + "/" + key).once("value", function (wanted) {

        document.getElementById("imgFormWanted").src = wanted.val().imagen;
        document.getElementById("inputWantedText").value = wanted.val().texto;

        switch (type) {
            case ("Inactivo"): document.getElementById("inputWantedOn").selectedIndex = 1; break;
            default: document.getElementById("inputWantedOn").selectedIndex = 0; break;
        }
    });
}

//borra el wanted de la BD-------------------------------------------------------------------
function removeWanted(type, key) {
    alert("funcion sin terminar");
}