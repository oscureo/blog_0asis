
var paginationLenght = 10;//Numero de tarjetas que se traen por peticion
var offsetPaginationKey = "-";//Ultima clave que se trajo de BD para poder hacer la paginacion
var nextColumn = 0;//0 o par = Columna Izq, impar = Columna Der
var tipoFiltro = "-"; //indica el tipo de filtro de la tabla actual
var databaseRef; //referencia a la base de datos
var isUserFilter = false;

function init() {
    initFirebase();
    databaseRef = database.ref("tarjetas");
    var filtroAux = new URLSearchParams(window.location.search).get('filtro');
    if (filtroAux == "" || filtroAux == undefined || filtroAux == null) {
        cargarTarjetasSinFiltro();
    } else {
        cargarTarjetasConFiltro(filtroAux, false);
    }
    showfiltersTags();
}

//Carga las ultimas tarjetas en orden temporal
function cargarTarjetasSinFiltro() {
    var firstTime = tipoFiltro != "";
    if (firstTime) {
        hideLeftMenu();
        emptyTable();
        tipoFiltro = "";
        databaseRef.off();
    }

    databaseRef = database.ref("tarjetas").orderByKey().limitToFirst(paginationLenght).startAt(offsetPaginationKey);
    document.getElementById("loadImg").hidden = false;
    databaseRef.once("value", function (data) {
        var hadchange = false;
        data.forEach(function (card) {
            if (firstTime) {
                //inserta la tarjeta
                hadchange = true;
                var name = getTitleCard(card);
                addCard(card.child("imagenes").val(), card.key, name);
                offsetPaginationKey = card.key;

            } else {
                firstTime = true;
            }
        });
        document.getElementById("loadImg").hidden = true;
        document.getElementById("ArrowMoreCards").hidden = !hadchange;
        document.getElementById("notCardsText").hidden = document.getElementById("tableCardsBody").getElementsByTagName("td")[0].childNodes.length != 0;
    });
}

//Carga las tarjetas en orden temporal aplicandole el filtro 'wordfilter'
function cargarTarjetasConFiltro(wordfilter, userFilter) {
    var firstTime = tipoFiltro != wordfilter;
    if (firstTime) {
        hideLeftMenu();
        emptyTable();
        tipoFiltro = wordfilter;
        databaseRef.off();
    }
    isUserFilter = userFilter

    if (userFilter) {
        databaseRef = database.ref("usuarios/" + wordfilter + "/tarjetas").orderByKey().limitToFirst(paginationLenght).startAt(offsetPaginationKey);
    } else {
        databaseRef = database.ref("FiltrosPorTipo/" + wordfilter).orderByKey().limitToFirst(paginationLenght).startAt(offsetPaginationKey);
    }
    document.getElementById("loadImg").hidden = false;
    document.getElementById("ArrowMoreCards").hidden = true;
    databaseRef.once("value", function (data) {
        var hadchange = false;
        var arrayPromise = new Array();
        data.forEach(function (tarjetaID) {

            var cardRef = database.ref("tarjetas/" + tarjetaID.key);
            arrayPromise.push(new Promise(function (resolve) {
                cardRef.once("value", function (card) {
                    if (firstTime) {
                        //insertar tarjeta
                        hadchange = true;
                        var name = getTitleCard(card);
                        addCard(card.child("imagenes").val(), card.key, name);
                        offsetPaginationKey = card.key;
                        resolve();
                    } else {
                        firstTime = true;
                        resolve();
                    }
                });
            }));
        });
        Promise.all(arrayPromise).then(function (value) {
            document.getElementById("loadImg").hidden = true;
            document.getElementById("ArrowMoreCards").hidden = !hadchange;
            document.getElementById("notCardsText").hidden = document.getElementById("tableCardsBody").getElementsByTagName("td")[0].childNodes.length != 0;
        });
    });
}

//Vacia la tabla de tarjetas
function emptyTable() {
    document.getElementById("notCardsText").hidden = true;
    document.getElementById("tableCardsBody").innerHTML = "<td></td><td></td>";
    offsetPaginationKey = "-";
    nextColumn = 0;
    window.scrollY = 0;
}

//inserta una nueva tarjeta en la tabla
function addCard(imgURL, key, name) {
    document.getElementById("tableCardsBody").getElementsByTagName("td")[nextColumn % 2].innerHTML += '<div onclick="gotoCardPage( \'' + key + '\' ); " class="card">\
                <img src="'+ imgURL + '"  /><br/>\
                <p>'+ name + '</p>\
            </div>';
    nextColumn++;
}

//Carga el siguiente bloque de paginacion de la actual tabla
function loadMoreCards() {
    if (tipoFiltro == "") {
        cargarTarjetasSinFiltro();
    } else {
        cargarTarjetasConFiltro(tipoFiltro, isUserFilter);
    }
}

//Dirige a la pagina donde se muestra la informacion de la tarjeta
function gotoCardPage(keyCard) {
    if (document.getElementById("leftMenu").hidden) {
        window.open("cardPage.html?keyCard=" + keyCard, "");
    }

}

//Dirige a la pagina donde se gestionan las tarjetas
function gotoManagerCards() {
    if (document.getElementById("leftMenu").hidden) {
        window.open("managerCards.html", "_self");
    }
}

//Si se detecta que se pulsa enter en el campo de busqueda
//se realiza la busqueda. (Atajo)
function enterSearch(event) {
    (event.keyCode) ? k = event.keyCode : k = event.which;
    // Si la tecla pulsada es enter (codigo ascii 13)
    if (k == 13) {
        // Si la variable id contiene "submit" enviamos el formulario
        searchCards();
    }
}

//Muestra la barra de busqueda si esta oculta
//si no esta oculta ejecuta la busqueda por filtro de la BD
function searchCards() {
    if (!document.getElementById("inputSearch").hidden) {
        //Realizamos la busqueda
        var textFilter = document.getElementById("inputSearch").value;
        if (textFilter.trim() != "") {
            hideLeftMenu();
            emptyTable();
            tipoFiltro = "-";
            document.getElementById("ArrowMoreCards").hidden = true;
            document.getElementById("loadImg").hidden = true;
            textFilter=removeAccents(textFilter.toLowerCase());
            databaseRef.off();
            databaseRef = database.ref("tarjetas");
            databaseRef.once("value", function (data) {
                data.forEach(function (card) {
                    card.forEach(function (info) {
                        if (info.key != "imagenes" && info.key != "edad") {
                            if (removeAccents(info.val().toLowerCase()).includes(textFilter)) {
                                //insertar tarjeta
                                var name = getTitleCard(card);
                                addCard(card.child("imagenes").val(), card.key, name);
                                return true;
                            }
                        }
                    });
                });
                document.getElementById("loadImg").hidden = true;
                document.getElementById("notCardsText").hidden = document.getElementById("tableCardsBody").getElementsByTagName("td")[0].childNodes.length != 0;
            });
        }
    } else {
        document.getElementById("inputSearch").hidden = false;
        document.getElementById("filtersearchDiv").hidden = false;
    }
}

//muestra los botones de filtros
function showfiltersTags() {
    //etiquetas
    database.ref("FiltrosPorTipo").once("value", function (data) {
        data.forEach(function (tagname) {
            var btnColor = "btn-warning";
            if (tagname.key == "Personaje" || tagname.key == "Artefacto" || tagname.key == "Criatura") {
                btnColor = "btn-primary";
            } else if (tagname.key == "PJ") {
                btnColor = "btn-light";
            }
            document.getElementById("filtersearchDiv").innerHTML += '<input type="button" value="' + tagname.key + '" class="btn ' + btnColor + '" onclick="cargarTarjetasConFiltro(\'' + tagname.key + '\',false)"/>';
        });
    });

    //Usuarios
    database.ref("usuarios").once("value", function (data) {
        data.forEach(function (user) {
            if (user.val.apodo != "") {
                document.getElementById("filtersearchDiv").innerHTML += '<input type="button" value="' + user.val().apodo + '" class="btn btn-secondary" onclick="cargarTarjetasConFiltro(\'' + user.key + '\',true)"/>';
            }
        });
    });

}

//Abre el servicio de Google Auth y registra o logea al usuario
function logIn() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().languageCode = 'es';
    firebase.auth().signInWithPopup(provider).then(function (result) {
        var token = result.credential.accessToken;
        var id = result.user.uid;
        var email = result.additionalUserInfo.profile.email;
        database.ref("usuarios").once("value", function (data) {
            if (data.hasChild(id)) {
                //Log In
                alert("Ya estas registrado");
            } else {
                //Sign In
                console.log("Sign in");
                var rightValue = false;
                var cont = 0;
                var pin;
                while (!rightValue && cont < 5) {
                    pin = prompt("Bienvenido a mi Blog de Rol: Ecribe tu apodo para darte de alta", "");
                    //No es identico a ninguno
                    var foundEqualNickName = false;
                    data.forEach(function (user) {
                        foundEqualNickName = foundEqualNickName || (user.val().apodo != "" && user.val().apodo == pin);
                    });
                    if (!foundEqualNickName) {
                        //No es vacio
                        //No contiene caracteres raros
                        rightValue = pin.trim() != "" && pin.indexOf("/") == -1 && pin.indexOf("\"") == -1
                            && pin.indexOf("=") == -1 && pin.indexOf("\\") == -1;
                        if (!rightValue) {
                            alert("Evita los caracteres raros porfavor");
                        }
                        cont++;
                    } else {
                        alert("Ese apodo ya lo tiene otro usuario");
                    }
                }
                if (cont >= 5) {
                    alert("El numero de intentos de Registro se ha superado, asegurate de permitir que te salten dialogos para poder rellenar el formulario.");
                }
                if (rightValue) {
                    alert("Cuenta creada con exito, pasalo bien " + pin);
                    database.ref("usuarios").child(id).set({
                        "apodo": pin,
                        "correo": email
                    });
                }
            }
        });

    }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
        console.log(error);
    });
}

//Quita tildes y acentos
const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }